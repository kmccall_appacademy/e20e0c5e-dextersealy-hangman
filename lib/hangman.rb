class Hangman
  attr_reader :guesser, :referee, :board

  def initialize(players)
    @referee = players[:referee]
    @guesser = players[:guesser]
    @board  = nil
  end

  def setup
    word_length = referee.pick_secret_word
    guesser.register_secret_length(word_length)
    @board = [nil] * word_length
  end

  def take_turn
    letter = guesser.guess(board)
    indices = referee.check_guess(letter)
    update_board(indices, letter)
    guesser.handle_response(letter, indices)
  end

  def update_board(indices, letter)
    indices.map { |idx| board[idx] = letter }
  end

  def render_board
    puts board.map { |el| el.nil? ? "_" : el }.join(" ")
  end

  def game_over?
    board.count(nil) == 0
  end

  def play
    setup
    until game_over? do
      render_board
      take_turn
    end
  end

end

class HumanPlayer

  def pick_secret_word
    print "Enter the length of your word: "
    gets.chomp.to_i
  end

  def check_guess(letter)
    print "Where does the letter #{letter.upcase} appear: "
    input = gets.chomp

    return [] unless input.length > 0
    input.split(",").map { |str| str.to_i - 1 }
  end

  def register_secret_length(word_length)
    puts "The secret word has #{word_length} letters."
  end

  def guess(board)
    print "Guess a letter from a to z: "
    gets.splice(0).downcase
  end

  def handle_response(letter, indices)
  end

end

class ComputerPlayer

  def initialize(dictionary)
    @dictionary = dictionary
    @secret_word = nil
    @word_length = nil
    @word_pattern = nil
    @guesses = ""
  end

  def pick_secret_word
    @secret_word = @dictionary.sample
    @secret_word.length
  end

  def check_guess(letter)
    letters = @secret_word.chars
    letters.map.with_index { |el, idx| el == letter ? idx : nil }.compact
  end

  def register_secret_length(word_length)
    @word_length = word_length
    @word_pattern = "." * word_length
  end

  def guess(board)
    guesses = board ? board.compact.uniq.join("") : @guesses

    letters = Hash.new(0)
    candidate_words.map do |word|
      word.chars.each { |chr| letters[chr] += 1 if !guesses.include?(chr) }
    end

    letters.sort { |a, b| a[1] <=> b[1] }[-1][0]
  end

  def handle_response(letter, indices)
    indices.each { |idx| @word_pattern[idx] = letter }
    @guesses += letter
  end

  def candidate_words
    return @dictionary unless @word_pattern

    pattern = @word_pattern
    if @guesses != ""
      pattern = @word_pattern.chars.map do |chr|
        (chr == ".") ? "[^#{@guesses}]" : chr
      end.join
    end

    words = @dictionary.select do |word|
      (word.length == @word_length) && !word.match(pattern).nil?
    end
  end

end

def load_dictionary
  File.readlines("lib/dictionary.txt").map(&:chomp)
end

if __FILE__ == $PROGRAM_NAME
  computer = ComputerPlayer.new(load_dictionary)
  human = HumanPlayer.new

  game = Hangman.new({referee: human, guesser: computer})

  game.play
end
